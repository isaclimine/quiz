﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{


    public Text questionDisplayText;
    public Text scoreDisplayText;
    public Text highScoreDisplayText;
    public Text timeRemainingDisplayText;
    public SimpleObjectPool answerButtonObjectPool;
    public Transform answerButtonParent;
    public GameObject questionDisplay;
    public GameObject roundEndDisplay;

    private DataController dataController;
    private RoundData currentRoundData;
    private QuestionData[] questionPool;

    private bool isRoundActive;
    private float timeRemaining;
    private int questionIndex;
    private int playerScore;
    private List<GameObject> answerButtonGameObjects = new List<GameObject>();

    // Use this for initialization
    void Start()
    {
        dataController = FindObjectOfType<DataController>();

        currentRoundData = dataController.GetCurrentRoundData();
        questionPool = currentRoundData.results;
        timeRemaining =(int) Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("timeToCompleteLevel_int").DoubleValue;
        UpdateTimeRemainingDisplay();

        playerScore = 0;
        questionIndex = 0;

        ShowQuestion();
        isRoundActive = true;

    }

    private void ShowQuestion()
    {
        RemoveAnswerButtons();
        QuestionData questionData = questionPool[questionIndex];
        questionDisplayText.text = questionData.question;



        AnswerData[] allAnswers = new AnswerData[questionData.incorrect_answers.Length];

        //set incorrect answers
        AnswerData[] incorrectAnswer = new AnswerData[questionData.incorrect_answers.Length];

        for (int i = 0; i < incorrectAnswer.Length; i++)
        {
            AnswerData newAnswer = new AnswerData
            {
                answerText = questionData. incorrect_answers[i],
                isCorrect = false
            
            };
            incorrectAnswer[i] = newAnswer;

        }
        for (int i = 0; i < allAnswers.Length; i++)
        {
            allAnswers = incorrectAnswer;
        }


        //set correct answer
        AnswerData correctAnswer = new AnswerData
        {
            answerText = questionData.correct_answer,
            isCorrect = true
        };


        AnswerData[] temp = new AnswerData[allAnswers.Length+1];
        allAnswers.CopyTo(temp, 0);
        allAnswers = temp;

        allAnswers[allAnswers.Length - 1] = correctAnswer;

        for (int i = 0; i < allAnswers.Length; i++)
        {
            GameObject answerButtonGameObject = answerButtonObjectPool.GetObject();
            answerButtonGameObjects.Add(answerButtonGameObject);
            answerButtonGameObject.transform.SetParent(answerButtonParent);

            AnswerButton answerButton = answerButtonGameObject.GetComponent<AnswerButton>();
            answerButton.Setup(allAnswers[i]);
        }
    }

    private void RemoveAnswerButtons()
    {
        while (answerButtonGameObjects.Count > 0)
        {
            answerButtonObjectPool.ReturnObject(answerButtonGameObjects[0]);
            answerButtonGameObjects.RemoveAt(0);
        }
    }

    public void AnswerButtonClicked(bool isCorrect)
    {
        if (isCorrect)
        {
            playerScore += 1;
            scoreDisplayText.text = "Score: " + playerScore.ToString();
        }
        else
        {
            if (playerScore > 0)
            {
                playerScore -= 1;
                scoreDisplayText.text = "Score: " + playerScore.ToString();
            }
        }

        if (questionPool.Length > questionIndex + 1)
        {
            questionIndex++;
            ShowQuestion();
        }
        else
        {
            EndRound();
        }

    }

    public void EndRound()
    {
        isRoundActive = false;

        dataController.SubmitNewHighestScore(playerScore);
        highScoreDisplayText.text = dataController.GetHighestScore().ToString();

        questionDisplay.SetActive(false);
        roundEndDisplay.SetActive(true);

        //submit to analytics
        Firebase.Analytics.FirebaseAnalytics
  .LogEvent("progress", "score", playerScore);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("MenuScreen");
        FindObjectOfType<DataController>().LoadDB();
    }
    public void RestartGame()
    {
        SceneManager.LoadScene("Game");
        FindObjectOfType<DataController>().LoadDB();
    }

    private void UpdateTimeRemainingDisplay()
    {
        timeRemainingDisplayText.text = "Time: " + Mathf.Round(timeRemaining).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (isRoundActive)
        {
            timeRemaining -= Time.deltaTime;
            UpdateTimeRemainingDisplay();

            if (timeRemaining <= 0f)
            {
                EndRound();
            }

        }
    }
}