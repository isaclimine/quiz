﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System;
using UnityEngine.UI;

public class RemoteData : MonoBehaviour
{
    public Text debugText;
    protected bool isFirebaseInitialized = false;
    Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
    // Use this for initialization
    void Start()
    {
        StartFireBase();
        FetchDataAsync();

        Firebase.Analytics.FirebaseAnalytics
  .LogEvent(Firebase.Analytics.FirebaseAnalytics.EventLogin);

    }
    void StartFireBase()
    {
 
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
               InitializeFirebase();
              
            }
            else
            {
                Debug.LogError (
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    void InitializeFirebase()
    {
        System.Collections.Generic.Dictionary<string, object> defaults =
          new System.Collections.Generic.Dictionary<string, object>();

        defaults.Add("timeToCompleteLevel_int", 30);

        Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
        print("RemoteConfig configured and ready!");
        isFirebaseInitialized = true;
    }

    // Start a fetch request...
    public Task FetchDataAsync()
    {
        print("Fetching data...");

        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(
            TimeSpan.Zero);
        return fetchTask.ContinueWith(FetchComplete);
    }
    void FetchComplete(Task fetchTask)
    {
        if (fetchTask.IsCanceled)
        {
            print("Fetch canceled.");
        }
        else if (fetchTask.IsFaulted)
        {
            print("Fetch encountered an error.");
        }
        else if (fetchTask.IsCompleted)
        {
            print("Fetch completed successfully!");
            Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();

        }
    }

   
}
