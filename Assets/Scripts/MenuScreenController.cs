﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScreenController : MonoBehaviour {
    public Text debugText;
public void StartGame()
    {
        print("loading scene...");
        SceneManager.LoadScene("Game");
    }

    public void Debug()
    {
      debugText.text =  Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("timeToCompleteLevel_int").LongValue.ToString();
    }
}
