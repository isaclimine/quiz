﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoundData  {
    public int response_code;
    public QuestionData[] results;
}

