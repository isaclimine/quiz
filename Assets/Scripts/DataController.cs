﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;

public class DataController : MonoBehaviour
{
    public RoundData roundData;

    private Progress progress;

    private string gameDataFileName = "data.json";


    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(gameObject);

        LoadDB();
        LoadProgress();
        SceneManager.LoadScene("MenuScreen");
    }
    public RoundData GetCurrentRoundData()
    {
        return roundData;
    }

    private void LoadProgress()
    {
        progress = new Progress();

        if (PlayerPrefs.HasKey("highestProgress"))
        {
            progress.highestScore = PlayerPrefs.GetInt("highestProgress");
        }
    }
    private void SaveProgress()
    {
        PlayerPrefs.SetInt("highestProgress", progress.highestScore);
    }
    public void SubmitNewHighestScore(int newScore)
    {
        print(newScore);
        if (newScore > progress.highestScore)
        {
            progress.highestScore = newScore;
            SaveProgress();
        }

    }
    public int GetHighestScore()
    {
        return progress.highestScore;
    }
    public void LoadDB()
    {
        string url = "https://opentdb.com/api.php?amount=10";
        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www));
    }
    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            LoadGameData(www.text);

            // Debug.Log("WWW Result!: " + www.text);// contains all the data sent from the server
        }
        else
        {
            Debug.LogError("WWW Error: " + www.error);
        }
    }
    private void LoadGameData(string text)
    {
        // string filePath = Path.Combine(Application.streamingAssetsPath, gameDataFileName);


        string dataAsJson = text;
        //File.ReadAllText(filePath);
        RoundData loadedData = JsonUtility.FromJson<RoundData>(dataAsJson);
        print(loadedData.response_code.ToString());

        roundData = loadedData;


    }

}