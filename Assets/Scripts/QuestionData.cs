﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestionData {
    public string category;
    public string type;
    public string sifficulty;

    public string question;
    public string correct_answer;
    public string[] incorrect_answers;
}
