﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LoadData : MonoBehaviour {

    void Start()
    {
        string url = "https://opentdb.com/api.php?amount=1";
        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www));
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Result!: " + www.text);// contains all the data sent from the server
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
        }
    }
}