﻿using System;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;

public class RemoteSettingsTest : MonoBehaviour
{
    public int time;
    bool loadedData;

    Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
    private void Update()
    {
        if (!loadedData)
        {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    loadedData = true;
                }
                else
                {
                    Debug.LogError(
                      "Could not resolve all Firebase dependencies: " + dependencyStatus);
                }
            });
        }
        else
        {
            System.Collections.Generic.Dictionary<string, object> defaults =
     new System.Collections.Generic.Dictionary<string, object>();

            defaults.Add("time_int", 22);

            Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
        }
        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(
            TimeSpan.Zero);
        //return fetchTask.ContinueWith(FetchComplete);
    }

    void FetchComplete(Task fetchTask)
    {
        if (fetchTask.IsCanceled)
        {
            // DebugLog("Fetch canceled.");
        }
        else if (fetchTask.IsFaulted)
        {
            // DebugLog("Fetch encountered an error.");
        }
        else if (fetchTask.IsCompleted)
        {
            // DebugLog("Fetch completed successfully!");
        }
    }
}
